import argparse
import logging
import os

import numpy as np
import torch
import torch.nn.functional as F
from PIL import Image
from torchvision import transforms

from unet import UNet
from utils.data_vis import plot_img_and_mask
from utils.dataset import BasicDataset


def predict_img(net,
				full_img,
				device,
				scale_factor=1,
				out_threshold=0.5):
	net.eval()

	img = torch.from_numpy(BasicDataset.preprocess(full_img, scale_factor))

	img = img.unsqueeze(0)
	img = img.to(device=device, dtype=torch.float32)

	with torch.no_grad():
		output = net(img)

		if net.n_classes > 1:
			probs = F.softmax(output, dim=1)
		else:
			probs = torch.sigmoid(output)

		probs = probs.squeeze(0)

		tf = transforms.Compose(
			[
				transforms.ToPILImage(),
				transforms.Resize(full_img.size[1]),
				transforms.ToTensor()
			]
		)

		probs = tf(probs.cpu())
		full_mask = probs.squeeze().cpu().numpy()

	return full_mask > out_threshold


def mask_to_image(mask):
	return Image.fromarray((mask * 255).astype(np.uint8))


def predict(net, input, maskThreshold, scale):

	"""
	Returns predictions based on variables:
		net: the network we're using to make predictions
		input: a list of input images
		maskThreshold: minimum probability value to consider a mask pixel white, default 0.5
		scale: scale factor for the input images, default 0.5

	"""


	# net = torch.hub.load('milesial/Pytorch-UNet', 'unet_carvana')



	device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
	logging.info(f'Using device {device}')

	logging.info("Model loaded !")

	predictions = []

	for i, img in enumerate(input):
		logging.info("\nPredicting image " + str(i) + "...")

		mask = predict_img(net=net,
						   full_img=img,
						   scale_factor=scale,
						   out_threshold=maskThreshold,
						   device=device)

		predictions.append(mask_to_image(mask))

	return predictions

