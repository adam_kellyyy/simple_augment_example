import sys
import numpy as np
import cv2
import os
import openpyxl

def dice(im1, im2):
	"""
	Computes the Dice coefficient, a measure of set similarity.
	Parameters
	----------
	im1 : array-like, bool
		Any array of arbitrary size. If not boolean, will be converted.
	im2 : array-like, bool
		Any other array of identical size. If not boolean, will be converted.
	Returns
	-------
	dice : float
		Dice coefficient as a float on range [0,1].
		Maximum similarity = 1
		No similarity = 0
		
	Notes
	-----
	The order of inputs for `dice` is irrelevant. The result will be
	identical if `im1` and `im2` are switched.
	"""
	im1 = np.asarray(im1).astype(np.bool)
	im2 = np.asarray(im2).astype(np.bool)

	if im1.shape != im2.shape:
		raise ValueError("Shape mismatch: im1 and im2 must have the same shape.")

	# Compute Dice coefficient
	intersection = np.logical_and(im1, im2)

	return 2. * intersection.sum() / (im1.sum() + im2.sum())
	
def evaluate(predictedList, actualList):

	total = 0
    
	for i in range(len(predictedList)):

		predicted = predictedList[i]
		actual = actualList[i]

		dice_coefficient = dice(predicted, actual)

		i = i + 1
		total = total + dice_coefficient

	average = total/i

	print('Result: ', str(average))
	return average