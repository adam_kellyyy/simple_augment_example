from . import datasetGenerator
import numpy as np
from PIL import Image
import time

defaultOrg = []
defaultMask = []

def setDataset(inputOrgs, inputMasks):
	global defaultOrg, defaultMask
	defaultOrg = inputOrgs
	defaultMask = inputMasks


class Run:
	def __init__(self, result, variables):
		self.result = result
		self.variables = []
		for var in variables:
			self.variables.append(var)


def search(trainFunction, seconds):

	possibleContrast = [0.2, 0.6, 1, 1.4, 1.8]
	possibleDefinition = [0, 0.5, 1, 1.5, 2]
	possibleRotation = [-135, -90, -45, 0, 45, 80, 135]
	possibleHeight = [1, 1.1, 1.2]
	possibleWidth = [1, 1.1, 1.2]
	possibleFlipOrientation = ['none', 'horizontal', 'vertical']

	variables = [2, 2, 3, 0, 0, 0]
	maxVals = [4, 4, 6, 2, 2, 2]
	minVals = [0, 0, 0, 0, 0, 0]


	runs = []
	timer = time.time() + seconds

	

	initialResult = trainFunction(defaultOrg, defaultMask)

	highest = 0

	currentVar = 0

	resUpper = 0
	resLower = 0

	while timer > time.time():
		if currentVar > 5:
			currentVar = 0

		upper = variables[currentVar] + 1
		lower = variables[currentVar] - 1

		skip = False

		if upper > maxVals[currentVar]:
			upper = upper - 1
			skip = True
		
		if lower < minVals[currentVar]:
			lower = lower + 1
			skip = True

		if not skip:

			tempVars = []
			for v in variables:
				tempVars.append(v)

			tempVars[currentVar] = upper
			augmentedOrg, augmentedMask = datasetGenerator.generateDataset(defaultOrg, defaultMask, possibleContrast[tempVars[0]], possibleDefinition[tempVars[1]], possibleRotation[tempVars[2]], possibleHeight[tempVars[3]], possibleWidth[tempVars[4]], possibleFlipOrientation[tempVars[5]])
			augmentedOrg = defaultOrg + augmentedOrg
			augmentedMask = defaultMask + augmentedMask
			resUpper = trainFunction(augmentedOrg, augmentedMask)
			runs.append(Run(resUpper, tempVars))

			tempVars[currentVar] = lower
			augmentedOrg, augmentedMask = datasetGenerator.generateDataset(defaultOrg, defaultMask, possibleContrast[tempVars[0]], possibleDefinition[tempVars[1]], possibleRotation[tempVars[2]], possibleHeight[tempVars[3]], possibleWidth[tempVars[4]], possibleFlipOrientation[tempVars[5]])
			augmentedOrg = defaultOrg + augmentedOrg
			augmentedMask = defaultMask + augmentedMask
			resLower = trainFunction(augmentedOrg, augmentedMask)
			runs.append(Run(resLower, tempVars))

			if resUpper > highest or resLower > highest:
				if resUpper > resLower:
					variables[currentVar] = upper
					highest = resUpper

				else:
					variables[currentVar] = lower
					highest = resLower

		currentVar += 1

	print('finished!')

	print('default dataset: ' + str(initialResult))
	print('best augmented dataset: ' + str(highest))

	print('parameters: contrast {}, definition {}, rotation {}, height {}, width {}, flipOrientation {}'.format(possibleContrast[variables[0]], possibleDefinition[variables[1]], possibleRotation[variables[2]], possibleHeight[variables[3]], possibleWidth[variables[4]], possibleFlipOrientation[variables[5]]))
