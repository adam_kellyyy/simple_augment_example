import os
import glob
from PIL import Image, ImageEnhance
import random
import sys
import shutil


def changeContrast(ima, imb, factor):

	enhancer = ImageEnhance.Contrast(ima)
	ima_output = enhancer.enhance(factor)

	enhancer = ImageEnhance.Contrast(imb)
	imb_output = enhancer.enhance(factor)
	return ima_output, imb_output

def changeRes(ima, imb, res):
	size = res, res
	ima_resized = ima.resize(size, Image.ANTIALIAS)
	size = res, res
	imb_resized = imb.resize(size, Image.ANTIALIAS)
	return ima_resized, imb_resized

def rotate(ima, imb, degrees):
	rotateda = ima.rotate(degrees)
	rotatedb = imb.rotate(degrees)
	return rotateda, rotatedb

def stretchHeight(ima, imb, factor):
	height = ima.size[0]
	width = ima.size[1]
	imga = ima.resize((width, int(height*factor)), Image.ANTIALIAS)

	height = imb.size[0]
	width = imb.size[1]
	imgb = imb.resize((width, int(height*factor)), Image.ANTIALIAS)
	return imga, imgb

def stretchWidth(ima, imb, factor):
	height = ima.size[0]
	width = ima.size[1]
	imga = ima.resize((int(width*factor), height), Image.ANTIALIAS)

	height = imb.size[0]
	width = imb.size[1]
	imgb = imb.resize((int(width*factor), height), Image.ANTIALIAS)
	return imga, imgb

def changeDefinition(ima, imb, factor):
	enhancer = ImageEnhance.Sharpness(ima)
	im_outputa = enhancer.enhance(factor)

	enhancer = ImageEnhance.Sharpness(imb)
	im_outputb = enhancer.enhance(factor)
	return im_outputa, im_outputb

def flip(ima, imb, direction):
	if direction == "horizontal":
		outa = ima.transpose(Image.FLIP_LEFT_RIGHT)
		outb = imb.transpose(Image.FLIP_LEFT_RIGHT)
	elif direction == "vertical":
		outa = ima.transpose(Image.FLIP_TOP_BOTTOM)
		outb = imb.transpose(Image.FLIP_TOP_BOTTOM)
	else:
		outa = ima
		outb = imb
	return outa, outb

def generateDataset(orgImages, maskImages, contrast, definition, rotation, height, width, flipOrientation):

	"""
	Returns two lists of processed images according to the following parameters:
		contrast: An enhancement factor of 0.0 gives a solid grey image. A factor of 1.0 gives the original image
		definition: An enhancement factor of 0.0 gives a blurred image, a factor of 1.0 gives the original image, and a factor of 2.0 gives a sharpened image
		rotation: degrees clockwise
		height: multiplies by the original height
		width: multiplies by the original width
		flipOrientation: horizontal, vertical, none
	"""

	print("contrast - " + str(contrast))
	print("definition - " + str(definition))
	print("rotation - " + str(rotation))
	print("height - " + str(height))
	print("width - " + str(width))
	print("flipOrientation - " + str(flipOrientation))

	orgProcessed = []
	maskProcessed = []

	for j in range(10):

		for i in range(len(orgImages)):

			org = orgImages[i]
			mask = maskImages[i]

			pOrg, pMask = changeContrast(org, mask, contrast)
			pOrg, pMask = changeDefinition(pOrg, pMask, definition)
			pOrg, pMask = rotate(pOrg, pMask, rotation)
			pOrg, pMask = stretchHeight(pOrg, pMask, height)
			pOrg, pMask = stretchWidth(pOrg, pMask, width)
			pOrg, pMask = flip(pOrg, pMask, flipOrientation)

			orgProcessed.append(pOrg)
			maskProcessed.append(pMask)

	return orgProcessed, maskProcessed






	

