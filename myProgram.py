from simple_augment import simple_augment
from glob import glob
from PIL import Image
import train
import predict
import evaluate

def getEvaluation(orgs, masks):
	# train the network on the images passed to it
	net = train.train(orgs, masks, 5, 0.5, 10)

	# evaluate the performance of the model based on test images
	testOrg = []
	actual = []

	for f in glob('data/val_images/*.png'):
		testOrg.append(Image.open(f).convert('RGB'))

	for f in glob('data/val_masks/*.png'):
		actual.append(Image.open(f).convert('L'))

	predicted = predict.predict(net, testOrg, 0.5, 0.5)

	evaluation = evaluate.evaluate(predicted, actual)

	return evaluation

defaultOrg = []
defaultMask = []


for f in glob('data/train_images/*.png'):
	defaultOrg.append(Image.open(f).convert('RGB'))

for f in glob('data/train_masks/*.png'):
	defaultMask.append(Image.open(f).convert('L'))

simple_augment.setDataset(defaultOrg, defaultMask)

num_seconds = 1200
simple_augment.search(getEvaluation, num_seconds)

